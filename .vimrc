call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'junegunn/goyo.vim'
Plug 'jooize/vim-colemak'
Plug 'ap/vim-css-color'
Plug 'vimwiki/vimwiki'
Plug 'itchyny/lightline.vim'
Plug 'luochen1990/rainbow'
Plug 'majutsushi/tagbar'
Plug 'flazz/vim-colorschemes'
Plug 'morhetz/gruvbox'
Plug 'sheerun/vim-polyglot'
Plug 'lankavitharana/ballerina-vim'
Plug 'lanferna/vim-togglecursor'
Plug 'rust-lang/rust.vim'
Plug 'farfanoide/vim-kivy'
Plug 'glench/vim-jinja2-syntax'
Plug 'habamax/vim-polar'
Plug 'habamax/vim-godot'

call plug#end()

if has('termguicolors')
	set t_Co=256                    " Set if term supports 256 colors.
	let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
	let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
	" set termguicolors
endif

colorscheme gruvbox
let g:gruvbox_contrast_dark="hard"
let g:gruvbox_bold=0
set background=dark

set nocompatible
set noswapfile
set number relativenumber
set copyindent
set smarttab
set tabstop=4
set shiftwidth=4
set noexpandtab
set autoindent
set smartindent
set hlsearch
set laststatus=2
set noshowmode
set autoread
set noconfirm
set splitbelow splitright
set backspace=indent,eol,start
set wrap
set linebreak
syntax on

" map! <ESC>[5A <C-Up>
" map! <ESC>[5B <C-Down>
inoremap jt <esc>
nnoremap : ;
nnoremap ; :
nnoremap <C-e> g<Up>
nnoremap <C-n> g<Down>
nmap <F9> :TagbarToggle<CR>

let g:rainbow_active=1
let g:cssColorVimDoNotMessMyUpdatetime=1
filetype plugin on

au BufEnter *.wiki,*.md let g:gruvbox_bold=1
au BufEnter *.wiki,*.md RainbowToggleOff
