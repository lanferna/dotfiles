#!/bin/bash

xsetwacom --set 'Wacom Intuos PT M 2 Pen stylus' MapToOutput eDP-1
xsetwacom --set 'Wacom Intuos PT M 2 Finger touch' MapToOutput eDP-1
xsetwacom --set 'Wacom Intuos PT M 2 Pad pad' MapToOutput eDP-1
