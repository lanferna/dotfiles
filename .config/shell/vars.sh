#!/bin/bash

export PATH="$PATH:$HOME/scripts:$HOME/.local/bin:$HOME/appimage:$HOME/anaconda3/bin"
export EDITOR="vim"
export PAGER="/usr/bin/most"
#export TERMINAL="/usr/local/bin/st"
export TERMINAL="/usr/bin/alacritty"
export MANPAGER="/bin/sh -c \"col -b | vim --not-a-term -c 'set ft=man ts=8 nomod nolist noma' -\""
export COLORTERM="truecolor"
export QT_STYLE_OVERRIDE="kvantum"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DATA_DIRS="/usr/local/share:/usr/share:/var/lib/flatpak/exports/share:/home/lanferna/.local/share/flatpak/exports/share"
export XDG_CONFIG_DIRS="/etc/xdg"
