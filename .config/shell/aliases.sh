#!/bin/bash

# vim and vimwiki
alias v='vim'
alias tools='vim ~/vimwiki/knowledge/tools/tools.wiki'
alias tasks='vim ~/vimwiki/tasks/Tasks.wiki'

# git helpers
alias g='git'

# images
alias feh='feh --conversion-timeout 1'

# manage config repository
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME'
alias dotst='dotfiles status'
alias dotls='dotfiles ls-tree --full-tree -r HEAD'
alias dotadd='dotfiles add'
alias dotcom='dotfiles commit -m'

# ls helpers
alias l1='exa -1 --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias br='broot'
alias blender='flatpak run org.blender.Blender'
alias krita='flatpak run org.kde.krita'

# window swallowing
alias m='devour mpv'
alias s='devour sxiv .'
alias zat='devour zathura'

# haxe helpers
alias munit='haxelib run munit test'
alias newtest='haxelib run munit create'
