#!/bin/bash

xsetwacom --set 'Wacom Intuos PT M 2 Pen stylus' MapToOutput HDMI-1
xsetwacom --set 'Wacom Intuos PT M 2 Finger touch' MapToOutput HDMI-1
xsetwacom --set 'Wacom Intuos PT M 2 Pad pad' MapToOutput HDMI-1
