. ~/.config/shell/aliases.sh
. ~/.config/shell/vars.sh
. ~/.config/shell/remaps.sh
. ~/.config/shell/wacom-map-to-output.sh
#. ~/.config/shell/wacom-map-to-output-single-monitor.sh

#function fish_prompt
#	powerline-shell --shell bare $status
#end

zoxide init fish | source

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
if test -f /home/lanferna/anaconda3/bin/conda
    eval /home/lanferna/anaconda3/bin/conda "shell.fish" "hook" $argv | source
else
    if test -f "/home/lanferna/anaconda3/etc/fish/conf.d/conda.fish"
        . "/home/lanferna/anaconda3/etc/fish/conf.d/conda.fish"
    else
        set -x PATH "/home/lanferna/anaconda3/bin" $PATH
    end
end
# <<< conda initialize <<<

